// ----------------------------------------------------------------------------
// extends normal area proxy with additional spound playback ability
class CRadishWaterAreaProxy extends CRadishAreaProxy {
    // ------------------------------------------------------------------------
    private var waterEmitterTemplatePath: String;
    private var waterEmitter: CEntity;
    // ------------------------------------------------------------------------
    default waterEmitterTemplatePath = "dlc\modtemplates\radishquestui\misc\wateremitter.w2ent";
    // ------------------------------------------------------------------------
    public function setSettings(settings: SRadishLayerAreaData) {
        super.setSettings(settings);
        updateWaterEmitter(settings);
    }
    // ------------------------------------------------------------------------
    public function updateWaterEmitter(settings: SRadishLayerAreaData) {
    	var comp: CWaterComponent;

        if (!waterEmitter) {
        	spawnWaterEmitter();
        }
        comp = ((CWaterComponent)waterEmitter.GetComponentByClassName('CWaterComponent'));
        comp.height = settings.height;
        // TODO: Add local points update too
    }
    // ------------------------------------------------------------------------
    protected function spawnWaterEmitter() {
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource(waterEmitterTemplatePath, true);
        waterEmitter = theGame.CreateEntity(template, placement.pos, placement.rot);
        waterEmitter.AddTag('RADUI');
    }
    // ------------------------------------------------------------------------
    protected function destroyWaterEmitter() {
        waterEmitter.StopAllEffects();
        waterEmitter.Destroy();
    }
    // ------------------------------------------------------------------------
    protected function spawn() {
        super.spawn();
        spawnWaterEmitter();
    }
    // ------------------------------------------------------------------------
    public function destroy() {
        destroyWaterEmitter();
        super.destroy();
    }
    // ------------------------------------------------------------------------
    public function moveTo(newPlacement: SRadishPlacement) {
        super.moveTo(newPlacement);
        waterEmitter.TeleportWithRotation(placement.pos, placement.rot);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
