// ----------------------------------------------------------------------------
class CRadishQuestLayerWaterArea extends CRadishQuestLayerArea {
    // ------------------------------------------------------------------------
    default specialization = "water";
    // ------------------------------------------------------------------------
	protected function initProxy() {
        // use specialized proxy with sound playback ability
        proxy = new CRadishWaterAreaProxy in this;
        proxy.init(proxyTemplate, placement, this.initializedFromEntity);

        ((CRadishWaterAreaProxy)proxy).setSettings(settings);

        initBorderpoint();
    }
    // ------------------------------------------------------------------------
    public function initFromDbgInfos(entity: CEntity) {
        var bbox: Box;
        var comp: CWaterComponent;

        comp = ((CWaterComponent)entity.GetComponentByClassName('CWaterComponent'));
        comp.GetWorldPoints(settings.border);

        bbox = comp.GetBoundingBox();
        settings.height = MaxF(1.0, bbox.Max.Z - bbox.Min.Z);
        // super method spawns proxy therefore settings must already contain
        // correct placement
        settings.placement.pos = entity.GetWorldPosition();
        settings.placement.rot = entity.GetWorldRotation();

        super.initFromDbgInfos(entity);

        // default color
        setAppearance('blue');
    }
	public function cloneFrom(src: CRadishLayerEntity) {
        super.cloneFrom(src);

        // default color
        setAppearance('blue');
    }
    // ------------------------------------------------------------------------
    // definition creation
    // ------------------------------------------------------------------------
    public function asDefinition() : SEncValue {
        var entity, transform, components, waterarea, bpList: SEncValue;
        var i: int;

        // generic static entity with envarea component:
        //  .type: CGameplayEntity
        //  transform:
        //    pos: [-110.0, -200.0, 30.0]
        //    scale: [ 1.0, 1.0, 1.0]
        //  components:
        //    envcomponent:
        //      .type: CWaterComponent
        //      ...

        entity = encValueNewMap();
        transform = encValueNewMap();
        components = encValueNewMap();

        waterarea = encValueNewMap();

        // -- envarea
        // TODO distinguish somehow between game default (== unset) values
        encMapPush_str(".type", "CWaterComponent", waterarea);
		
		if (settings.height != 2.0) {
			encMapPush_float("height", settings.height, waterarea);
        } else {
            // add comment
            encMapPush_float("#height", 2.0, waterarea);
        }

        bpList = encValueNewList();
        for (i = 0; i < settings.border.Size(); i += 1) {
            encListPush(Pos4ToEncValue(settings.border[i] - placement.pos, true), bpList);
        }
        encMapPush("localPoints", bpList, waterarea);

        // -- transform
        encMapPush("pos", PosToEncValue(this.placement.pos), transform);
        encMapPush("scale", PosToEncValue(Vector(1.0, 1.0, 1.0)), transform);

        // -- components
        encMapPush("water", waterarea, components);

        // -- entity
        encMapPush_str(".type", "CGameplayEntity", entity);
        encMapPush("transform", transform, entity);
        encMapPush("components", components, entity);

        return entity;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------

